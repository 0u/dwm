/* Toggle the fullscreen for the current window. */
static void
fullscreen(const Arg *arg)
{
    if (mons != NULL && mons->sel != NULL)
        setfullscreen(mons->sel, !mons->sel->isfullscreen);
}
